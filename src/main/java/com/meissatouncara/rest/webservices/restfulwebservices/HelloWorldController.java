package com.meissatouncara.rest.webservices.restfulwebservices;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {
	
	//Step 03 - Creating a Hello World Service
	@GetMapping(path="/hello-world")
	public String helloWorld() {
		return "Hello World";		
	}
	//Step 04 - Enhancing the Hello World Service to return a Bean
	@GetMapping(path="/hello-world-bean")
	public HelloWorldBean helloWorldBean() {
		return new HelloWorldBean("Hello World") ;		
	}
	//Step 06 - Enhancing the Hello World Service with a Path Variablee
	@GetMapping(path="/hello-world/path-variable/{name}")
	public HelloWorldBean hellowordPathVaraible(@PathVariable String name) {
		return new HelloWorldBean(String.format("Hello Word, %s", name));
	}

}
